//
//  ActionButton.swift
//  push-to-talk
//
//  Created by David Sklenar on 10/11/18.
//  Copyright © 2018 David Sklenar. All rights reserved.
//

import UIKit

class ActionButton: CircularButton {
    init(title: String, backgroundColor: UIColor) {
        super.init(frame: .zero)

        showsTouchWhenHighlighted = true
        titleLabel?.font = .boldSystemFont(ofSize: 18)
        setTitle(title, for: .normal)
        setTitle("", for: .highlighted)
        setTitleColor(.gray, for: .highlighted)
        self.backgroundColor = backgroundColor
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CircularButton: UIButton {
    struct Dimension {
        static let size = CGFloat(100)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layer.cornerRadius = frame.size.width / 2.0
    }

    override var intrinsicContentSize: CGSize {
        return CGSize(width: Dimension.size, height: Dimension.size)
    }
}
