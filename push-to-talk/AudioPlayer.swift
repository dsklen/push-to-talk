//
//  AudioPlayer.swift
//  push-to-talk
//
//  Created by David Sklenar on 10/11/18.
//  Copyright © 2018 David Sklenar. All rights reserved.
//

import AVFoundation
import Foundation

struct AudioPlayer {
    // AVAudioPlayer unexpectedly gets deallocated right away, so adding an instance variable to
    // hold on to a reference each new instance.
    private var currentAudioPlayer: AVAudioPlayer?

    mutating func playFile(atURL URL: URL) {
        currentAudioPlayer =  try? AVAudioPlayer(contentsOf: URL)
        currentAudioPlayer?.play()
    }
}
