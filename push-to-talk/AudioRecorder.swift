//
//  AudioRecorder.swift
//  push-to-talk
//
//  Created by David Sklenar on 10/11/18.
//  Copyright © 2018 David Sklenar. All rights reserved.
//

import AVFoundation
import UIKit

/// A simple protocol to notify a delegate that audio recording completed, or failed due to an error.
protocol AudioRecorderDelegate: class {
    func didRecordAudio(toURL URL: URL)
    func didEncounterError()
}

/// A lightweight wrapper around AVFoundation's AVAudioRecorder that handles permissioins, initialization
/// and simplifies error states.
class AudioRecorder: NSObject {

    // MARK: Properties

    private let avFoundationAudioRecorder: AVAudioRecorder
    private weak var delegate: AudioRecorderDelegate?

    private static func makeAVAudioRecorder() -> AVAudioRecorder? {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        guard let documentsDirectoryURL = urls.first else { return nil }

        let outputURL = documentsDirectoryURL.appendingPathComponent("recording.m4a")

        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]

        return try? AVAudioRecorder(url: outputURL, settings: settings)
    }

    // MARK: Initialization

    init?(delegate: AudioRecorderDelegate) {
        guard let audioRecorder = AudioRecorder.makeAVAudioRecorder() else { return nil }

        self.avFoundationAudioRecorder = audioRecorder
        self.delegate = delegate

        super.init()

        self.avFoundationAudioRecorder.delegate = self
    }

    // MARK: Actions:

    func startRecording() {
        // According to the documentation, AVAudioRecorder should remove an audio file if it already exists
        // at the initialization path, but that doesn't seem to be the case, so deleting the recording just
        // to be sure.
        clearRecording()
        avFoundationAudioRecorder.record()
    }

    func stopRecording() {
        avFoundationAudioRecorder.stop()
    }

    func clearRecording() {
        avFoundationAudioRecorder.deleteRecording()
    }
}

// MARK: AVAudioRecorderDelegate

extension AudioRecorder: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if flag {
            delegate?.didRecordAudio(toURL: recorder.url)
        } else {
            delegate?.didEncounterError()
        }
    }

    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        delegate?.didEncounterError()
    }
}

// MARK: Permissions

extension AudioRecorder {
    static func requestMicrophonePermissions(fromViewController viewController: ViewController) {
        let recordingSession = AVAudioSession.sharedInstance()

        do {
            try recordingSession.setCategory(.playAndRecord, mode: .`default`)
            try recordingSession.setActive(true)

            recordingSession.requestRecordPermission { allowed in
                DispatchQueue.main.async {
                    if !allowed {
                        let alertController = UIAlertController(
                            title: "Permissions Error",
                            message: "You need to enable microphone permissions in in Settings > Privacy > Microphone",
                            preferredStyle: .alert
                        )

                        let action = UIAlertAction(title: "Go To Settings", style: .cancel) { _ in
                            if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                                UIApplication.shared.open(
                                    settingsURL,
                                    options: [:],
                                    completionHandler: nil
                                )
                            }
                        }

                        alertController.addAction(action)
                        viewController.present(alertController, animated: true, completion: nil)
                    }
                }
            }
        } catch {
            viewController.showFatalErrorMessage()
        }
    }
}

extension UIViewController {
    func showFatalErrorMessage() {
        let alertController = UIAlertController(
            title: "Error",
            message: "Unable to record audio on your device.",
            preferredStyle: .alert
        )

        let action = UIAlertAction(title: "Exit", style: .destructive) { _ in exit(0) }
        alertController.addAction(action)

        present(alertController, animated: true, completion: nil)
    }
}
