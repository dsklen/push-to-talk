//
//  Color.swift
//  push-to-talk
//
//  Created by David Sklenar on 10/11/18.
//  Copyright © 2018 David Sklenar. All rights reserved.
//

import UIKit

struct Color {
    static let darkGreen = UIColor(red: 79.0 / 255.0, green: 122.0 / 255.0, blue: 66.0 / 255.0, alpha: 1)
}
