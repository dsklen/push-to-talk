//
//  ViewController.swift
//  push-to-talk
//
//  Created by David Sklenar on 10/11/18.
//  Copyright © 2018 David Sklenar. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {

    // MARK: Helper Types

    enum State {
        case readyToRecord
        case recording
        case finishedRecording
    }

    // MARK: Properties

    private var audioRecorder: AudioRecorder! = nil
    private var audioPlayer = AudioPlayer()
    private var lastRecordingURL: URL?

    private let generator: UIImpactFeedbackGenerator = {
        let generator = UIImpactFeedbackGenerator(style: .medium)
        generator.prepare()
        return generator
    }()

    private var state = State.readyToRecord {
        didSet {
            if self.state != oldValue {
                updateStatusForCurrentState()

                if state != .recording {
                    updateButtonsForCurrentState()
                }
            }
        }
    }

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [statusLabel, recordButton])
        stackView.axis = .vertical
        stackView.spacing = 20
        stackView.layoutMargins = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        stackView.alignment = .center
        return stackView
    }()

    private let statusLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = .boldSystemFont(ofSize: 20)
        label.textColor = .black
        label.text = "Ready to Record"
        return label
    }()

    private let recordButton: UIButton = {
        let button = ActionButton(title: "Record", backgroundColor: Color.darkGreen)
        button.addTarget(self, action: #selector(startRecording), for: .touchDown)
        button.addTarget(self, action: #selector(stopRecording), for: .touchUpInside)
        button.addTarget(self, action: #selector(stopRecording), for: .touchUpOutside)
        button.addTarget(self, action: #selector(stopRecording), for: .touchDragExit)
        return button
    }()

    private let shareButton: UIButton = {
        let button = ActionButton(title: "Share", backgroundColor: .darkGray)
        button.addTarget(self, action: #selector(shareRecording), for: .touchUpInside)
        return button
    }()

    private let clearButton: UIButton = {
        let button = ActionButton(title: "Clear", backgroundColor: .red)
        button.addTarget(self, action: #selector(clearCurrentRecording), for: .touchUpInside)
        return button
    }()

    private let playbackButton: UIButton = {
        let button = ActionButton(title: "Replay", backgroundColor: .orange)
        button.addTarget(self, action: #selector(playbackCurrentRecording), for: .touchUpInside)
        return button
    }()

    // MARK: View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor(patternImage: UIImage(imageLiteralResourceName: "wavecut_@2X"))

        setUpViewHierarchy()
        setUpLayoutConstraints()
        setUpNotifications()

        AudioRecorder.requestMicrophonePermissions(fromViewController: self)

        guard let recorder = AudioRecorder(delegate: self) else { showFatalErrorMessage(); return }
        self.audioRecorder = recorder
    }

    private func setUpViewHierarchy() {
        view.addSubview(stackView)
    }

    private func setUpLayoutConstraints() {
        stackView.snp.makeConstraints { make in
            make.top.greaterThanOrEqualTo(view.safeAreaLayoutGuide).offset(20)
            make.leading.bottom.trailing.equalTo(view.safeAreaLayoutGuide).inset(20)
        }
    }

    private func setUpNotifications() {
        _ = NotificationCenter.default.addObserver(
            forName: UIApplication.willEnterForegroundNotification,
            object: nil,
            queue: .main,
            using: { _ in
                AudioRecorder.requestMicrophonePermissions(fromViewController: self)
            }
        )
    }

    private func updateButtonsForCurrentState() {
        let views: [UIView]

        switch state {
        case .readyToRecord, .recording: views = [statusLabel, recordButton]
        case .finishedRecording: views = [statusLabel, clearButton, playbackButton, shareButton]
        }

        stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        views.forEach { stackView.addArrangedSubview($0) }
    }

    private func updateStatusForCurrentState() {
        switch state {
        case .readyToRecord: statusLabel.text = "Ready to Record"
        case .recording:  statusLabel.text = "🎤 Recording..."
        case .finishedRecording:  statusLabel.text = "Recording Finished"
        }
    }

    // MARK: Actions

    @objc
    private func startRecording() {
        state = .recording
        generator.prepare()
        generator.impactOccurred()
        audioRecorder.startRecording()
    }

    @objc
    private func stopRecording() {
        audioRecorder.stopRecording()
        generator.impactOccurred()
    }

    @objc
    private func shareRecording() {
        guard let url = lastRecordingURL else { return }

        let activityController = UIActivityViewController(activityItems: [url], applicationActivities: nil)

        activityController.completionWithItemsHandler = { _, _, _, _ in
            self.state = .readyToRecord
        }

        present(activityController, animated: true, completion: nil)
    }

    @objc
    private func clearCurrentRecording() {
        state = .readyToRecord
        audioRecorder.clearRecording()
        lastRecordingURL = nil
    }

    @objc
    private func playbackCurrentRecording() {
        guard let url = lastRecordingURL else { return }
        audioPlayer.playFile(atURL: url)
    }
}

// MARK: AudioRecorderDelegate

extension ViewController: AudioRecorderDelegate {
    func didRecordAudio(toURL URL: URL) {
        lastRecordingURL = URL
        state = .finishedRecording
    }

    func didEncounterError() {
        showFatalErrorMessage()
    }
}
